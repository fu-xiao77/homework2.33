package com.hnevc.fuxiao;
import java.util.Scanner;

/**
 *
 * 年龄的最小公倍数。
 *动物园里新来了两只骆驼，计算出它们年龄的最小公倍数，
 * 从键盘输入两个整数，输出两个整数的最小公倍数。
 *
 */
public class homework33 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("输入第一个数：");
        int a = sc.nextInt();
        System.out.print("输入第二个数：");
        int b = sc.nextInt();
        //最小公倍数获取：a乘以b的乘积除以最大公约数即可
        int maxs = a*b/maxa(a,b);
        System.out.println(a+"和"+b+"的最小公倍数为:"+maxs);

    }
    public static int maxa(int a,int b){
        //如果a<b,两数交换位置
        if(a<b){
            int temp = a;
            a = b;
            b = temp;
        }
        //使用辗转相除法，只要被除数没有为0，求余数
        while(b > 0){
            int gys = a%b;
            a = b;
            b = gys;
        }
        //返回最大公约数
        return a;
    }
}
